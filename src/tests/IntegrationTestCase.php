<?php
declare(strict_types=1);

namespace App\Tests;

use App\MarsRoverMission\Infrastructure\Persistence\PDOFactory;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Dotenv\Dotenv;

class IntegrationTestCase extends KernelTestCase
{
    protected $db;
    protected \PDO $pdo;

    protected function setUp(): void
    {
        parent::setUp();

//        self::bootKernel(['environment' => 'test']);
//
//        $this->db = self::$container->get(\PDO::class);
//
//        $this->db->beginTransaction();

        (new Dotenv(true))->loadEnv(dirname(__DIR__) . '/.env');

        $this->pdo = PDOFactory::newConnectionDsn(
            'mysql:host=' . getenv('DATABASE_HOST') . ';dbname=' . getenv('DATABASE_NAME'),
            getenv('DATABASE_USER'),
            getenv('DATABASE_PASSWORD')
        );
        $this->pdo->beginTransaction();
    }

    protected function tearDown(): void
    {
        $this->pdo->rollBack();

        parent::tearDown();
    }

    protected function getService($id)
    {
        return self::$container->get($id);
    }

}
