<?php

namespace App\Tests\MarsRoverMission\Infrastructure\Domain\Model\Obstacle;

use App\MarsRoverMission\Domain\Model\Obstacle\Obstacle;
use App\MarsRoverMission\Domain\Model\Obstacle\ObstacleId;
use App\MarsRoverMission\Domain\Model\Obstacle\ObstaclePosition;
use App\MarsRoverMission\Infrastructure\Domain\Model\Obstacle\ObstacleRepositoryPDO;
use App\Tests\IntegrationTestCase;

/**
 * @group integration
 */
class ObstacleRepositoryPDOTest extends IntegrationTestCase
{
    private ObstacleRepositoryPDO $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new ObstacleRepositoryPDO($this->pdo);
        $this->repository->truncate();
    }

    /** @test */
    public function should_persist_obstacle()
    {
        $obstacle = Obstacle::create(
            ObstacleId::fromId('test'),
            ObstaclePosition::fromXY(0,0)
        );

        $this->repository->save($obstacle);
        $result = $this->repository->loadById($obstacle->obstacleId());

        $this->assertEquals($obstacle, $result);
    }

    /** @test */
    public function should_load_all_obstacles()
    {
        $obstacle1 = Obstacle::create(
            ObstacleId::fromId('test'),
            ObstaclePosition::fromXY(0,0)
        );
        $this->repository->save($obstacle1);
        $obstacle2 = Obstacle::create(
            ObstacleId::fromId('test2'),
            ObstaclePosition::fromXY(1,0)
        );
        $this->repository->save($obstacle2);
        $expected = [$obstacle1, $obstacle2];

        $result = $this->repository->loadAll();

        $this->assertEquals($expected, $result);
    }
}
