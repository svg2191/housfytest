<?php

namespace App\Tests\MarsRoverMission\Infrastructure\Domain\Model\Rover;

use App\MarsRoverMission\Domain\Model\Rover\Rover;
use App\MarsRoverMission\Domain\Model\Rover\RoverDirection;
use App\MarsRoverMission\Domain\Model\Rover\RoverId;
use App\MarsRoverMission\Domain\Model\Rover\RoverPosition;
use App\MarsRoverMission\Infrastructure\Domain\Model\Rover\RoverRepositoryPDO;
use App\Tests\IntegrationTestCase;

/**
 * @group integration
 */
class RoverRepositoryPDOTest extends IntegrationTestCase
{
    private RoverRepositoryPDO $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new RoverRepositoryPDO($this->pdo);
        $this->repository->truncate();
    }

    /** @test */
    public function should_save()
    {
        $rover = Rover::create(
            RoverId::fromId('test'),
            RoverDirection::fromDirection('N'),
            RoverPosition::fromXY(0,0)
        );
        $this->repository->save($rover);

        $roverFromDb = $this->repository->loadById($rover->roverId());
        $this->assertEquals($rover->roverId()->id(), $roverFromDb->roverId()->id());
    }

    /** @test */
    public function should_load_all()
    {
        $rover1 = Rover::create(
            RoverId::fromId('test'),
            RoverDirection::fromDirection('N'),
            RoverPosition::fromXY(0,0)
        );
        $this->repository->save($rover1);
        $rover2 = Rover::create(
            RoverId::fromId('test2'),
            RoverDirection::fromDirection('E'),
            RoverPosition::fromXY(1,0)
        );
        $this->repository->save($rover2);
        $expected = [$rover1, $rover2];

        $result = $this->repository->loadAll();

        $this->assertEquals($expected, $result);
    }

}
