<?php

namespace App\Tests\MarsRoverMission\Application\Service\Obstacle;

use App\MarsRoverMission\Application\Service\Obstacle\CreateObstacleCommand;
use App\MarsRoverMission\Application\Service\Obstacle\CreateObstacleResponse;
use App\MarsRoverMission\Application\Service\Obstacle\CreateObstacleUseCase;
use App\MarsRoverMission\Domain\Model\Obstacle\Obstacle;
use App\MarsRoverMission\Domain\Model\Obstacle\ObstacleId;
use App\MarsRoverMission\Domain\Model\Obstacle\ObstaclePosition;
use App\MarsRoverMission\Infrastructure\Domain\Model\Obstacle\ObstacleRepositoryPDO;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

class CreateObstacleUseCaseTest extends TestCase
{
    use ProphecyTrait;

    private \Prophecy\Prophecy\ObjectProphecy $repository;
    private CreateObstacleUseCase $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->prophesize(ObstacleRepositoryPDO::class);
        $this->service = new CreateObstacleUseCase($this->repository->reveal());
    }

    /** @test */
    public function should_save()
    {
        $position = json_encode(['x' => 0, 'y' => 0]);
        $command = new CreateObstacleCommand($position);
        $result = $this->service->execute($command);
        $expected = new CreateObstacleResponse(ObstacleId::fromId($command->ObstacleId()));
        $Obstacle = Obstacle::create(
            ObstacleId::fromId($command->ObstacleId()),
            ObstaclePosition::fromJson($position)
        );

        $this->repository->save($Obstacle)->shouldHaveBeenCalled();
        $this->assertEquals($expected, $result);
    }
}
