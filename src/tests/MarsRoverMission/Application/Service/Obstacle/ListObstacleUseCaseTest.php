<?php

namespace App\Tests\MarsRoverMission\Application\Service\Obstacle;

use App\MarsRoverMission\Application\Service\Obstacle\ListObstacleCommand;
use App\MarsRoverMission\Application\Service\Obstacle\ListObstacleResponse;
use App\MarsRoverMission\Application\Service\Obstacle\ListObstacleUseCase;
use App\MarsRoverMission\Domain\Model\Obstacle\Obstacle;
use App\MarsRoverMission\Domain\Model\Obstacle\ObstacleId;
use App\MarsRoverMission\Domain\Model\Obstacle\ObstaclePosition;
use App\MarsRoverMission\Infrastructure\Domain\Model\Obstacle\ObstacleRepositoryPDO;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

class ListObstacleUseCaseTest extends TestCase
{
    use ProphecyTrait;

    private \Prophecy\Prophecy\ObjectProphecy $repository;
    private ListObstacleUseCase $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->prophesize(ObstacleRepositoryPDO::class);
        $this->service = new ListObstacleUseCase($this->repository->reveal());
    }

    /** @test */
    public function should_return_all_Obstacle_list()
    {
        $command = new ListObstacleCommand();
        $Obstacle1 = Obstacle::create(
            ObstacleId::fromId('test-1'),
            ObstaclePosition::fromXY(15,60)
        );
        $Obstacle2 = Obstacle::create(
            ObstacleId::fromId('test-2'),
            ObstaclePosition::fromXY(0,0)
        );
        $repoReturn = [$Obstacle1, $Obstacle2];
        $this->repository->loadAll()->willReturn($repoReturn);
        $expected = new ListObstacleResponse(...$repoReturn);

        $result = $this->service->execute($command);

        $this->repository->loadAll()->shouldHaveBeenCalled();

        $this->assertEquals($expected, $result);
    }
}
