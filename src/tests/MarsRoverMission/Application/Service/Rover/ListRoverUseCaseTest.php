<?php

namespace App\Tests\MarsRoverMission\Application\Service\Rover;

use App\MarsRoverMission\Application\Service\Rover\ListRoverCommand;
use App\MarsRoverMission\Application\Service\Rover\ListRoverResponse;
use App\MarsRoverMission\Application\Service\Rover\ListRoverUseCase;
use App\MarsRoverMission\Domain\Model\Rover\Rover;
use App\MarsRoverMission\Domain\Model\Rover\RoverDirection;
use App\MarsRoverMission\Domain\Model\Rover\RoverId;
use App\MarsRoverMission\Domain\Model\Rover\RoverPosition;
use App\MarsRoverMission\Infrastructure\Domain\Model\Rover\RoverRepositoryPDO;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

class ListRoverUseCaseTest extends TestCase
{
    use ProphecyTrait;

    private \Prophecy\Prophecy\ObjectProphecy $repository;
    private ListRoverUseCase $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->prophesize(RoverRepositoryPDO::class);
        $this->service = new ListRoverUseCase($this->repository->reveal());
    }

    /** @test */
    public function should_return_all_rover_list()
    {
        $command = new ListRoverCommand();
        $rover1 = Rover::create(
            RoverId::fromId('test-1'),
            RoverDirection::fromDirection('N'),
            RoverPosition::fromXY(15,60)
        );
        $rover2 = Rover::create(
            RoverId::fromId('test-2'),
            RoverDirection::fromDirection('W'),
            RoverPosition::fromXY(0,0)
        );
        $repoReturn = [$rover1, $rover2];
        $this->repository->loadAll()->willReturn($repoReturn);
        $expected = new ListRoverResponse(...$repoReturn);

        $result = $this->service->execute($command);

        $this->repository->loadAll()->shouldHaveBeenCalled();

        $this->assertEquals($expected, $result);
    }
}
