<?php

namespace App\Tests\MarsRoverMission\Application\Service\Rover;

use App\MarsRoverMission\Application\Service\Rover\CreateRoverCommand;
use App\MarsRoverMission\Application\Service\Rover\CreateRoverResponse;
use App\MarsRoverMission\Application\Service\Rover\CreateRoverUseCase;
use App\MarsRoverMission\Domain\Model\Rover\Rover;
use App\MarsRoverMission\Domain\Model\Rover\RoverDirection;
use App\MarsRoverMission\Domain\Model\Rover\RoverId;
use App\MarsRoverMission\Domain\Model\Rover\RoverPosition;
use App\MarsRoverMission\Infrastructure\Domain\Model\Rover\RoverRepositoryPDO;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

class CreateRoverUseCaseTest extends TestCase
{
    use ProphecyTrait;

    private \Prophecy\Prophecy\ObjectProphecy $repository;
    private CreateRoverUseCase $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->prophesize(RoverRepositoryPDO::class);
        $this->service = new CreateRoverUseCase($this->repository->reveal());
    }

    /** @test */
    public function should_save()
    {
        $command = new CreateRoverCommand();
        $result = $this->service->execute($command);
        $expected = new CreateRoverResponse(RoverId::fromId($command->roverId()));
        $rover = Rover::create(
            RoverId::fromId($command->roverId()),
            RoverDirection::fromDirection('N'),
            RoverPosition::fromXY(0,0)
        );

        $this->repository->save($rover)->shouldHaveBeenCalled();
        $this->assertEquals($expected, $result);
    }
}
