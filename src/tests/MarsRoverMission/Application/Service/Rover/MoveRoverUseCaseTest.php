<?php

namespace App\Tests\MarsRoverMission\Application\Service\Rover;

use App\MarsRoverMission\Application\Service\Rover\MoveRoverCommand;
use App\MarsRoverMission\Application\Service\Rover\MoveRoverResponse;
use App\MarsRoverMission\Application\Service\Rover\MoveRoverUseCase;
use App\MarsRoverMission\Domain\Model\Obstacle\Obstacle;
use App\MarsRoverMission\Domain\Model\Obstacle\ObstacleId;
use App\MarsRoverMission\Domain\Model\Obstacle\ObstaclePosition;
use App\MarsRoverMission\Domain\Model\Rover\Rover;
use App\MarsRoverMission\Domain\Model\Rover\RoverDirection;
use App\MarsRoverMission\Domain\Model\Rover\RoverId;
use App\MarsRoverMission\Domain\Model\Rover\RoverPosition;
use App\MarsRoverMission\Infrastructure\Domain\Model\Rover\RoverRepositoryPDO;
use App\MarsRoverMission\Infrastructure\Domain\Model\Obstacle\ObstacleRepositoryPDO;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

class MoveRoverUseCaseTest extends TestCase
{
    use ProphecyTrait;

    private MoveRoverUseCase $service;
    private \Prophecy\Prophecy\ObjectProphecy $roverRepo;
    private \Prophecy\Prophecy\ObjectProphecy $obstacleRepo;

    protected function setUp(): void
    {
        parent::setUp();
        $this->roverRepo = $this->prophesize(RoverRepositoryPDO::class);
        $this->obstacleRepo = $this->prophesize(ObstacleRepositoryPDO::class);
        $this->service = new MoveRoverUseCase($this->roverRepo->reveal(), $this->obstacleRepo->reveal());
    }

    /** @test */
    public function should_save()
    {
        $command = new MoveRoverCommand('test',['F','F']);
        $roverId = RoverId::fromId('test');
        $initialRover = Rover::create($roverId, RoverDirection::fromDirection('N'), RoverPosition::fromXY(0,0,));
        $this->roverRepo->loadById($roverId)->willReturn($initialRover);
        $this->obstacleRepo->loadAll()->willReturn([]);
        $finalPosition = RoverPosition::fromXY(0,2,);
        $finalRover = Rover::create($roverId, RoverDirection::fromDirection('N'), $finalPosition);

        $result = $this->service->execute($command);
        $expected = new MoveRoverResponse($finalPosition, null);

        $this->roverRepo->update($finalRover)->shouldHaveBeenCalled();
        $this->assertEquals($expected, $result);
    }

    /** @test */
    public function should_stay_at_last_correct_position_if_find_obstacle()
    {
        $command = new MoveRoverCommand('test',['F','F']);
        $roverId = RoverId::fromId('test');
        $initialRover = Rover::create($roverId, RoverDirection::fromDirection('N'), RoverPosition::fromXY(0,0,));
        $this->roverRepo->loadById($roverId)->willReturn($initialRover);
        $obstacle = Obstacle::create(
            ObstacleId::fromId('test'),
            ObstaclePosition::fromXY(0,2)
        );
        $this->obstacleRepo->loadAll()->willReturn([$obstacle]);
        $finalPosition = RoverPosition::fromXY(0,1,);
        $finalRover = Rover::create($roverId, RoverDirection::fromDirection('N'), $finalPosition);

        $result = $this->service->execute($command);
        $expected = new MoveRoverResponse($finalPosition, $obstacle->position()->toJson());

        $this->roverRepo->update($finalRover)->shouldHaveBeenCalled();
        $this->assertEquals($expected, $result);
    }
}
