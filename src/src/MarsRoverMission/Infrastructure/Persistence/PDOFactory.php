<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Infrastructure\Persistence;

use PDO;

final class PDOFactory
{
    public static function newConnectionDsn($dsn, $username, $password): \PDO
    {
        $pdo = new PDO($dsn, $username, $password);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $pdo;
    }

    public static function newMysqlConnection($host, $dbname, $username, $password, $unixSocket): \PDO
    {
        $dsn = ($unixSocket !== '') ? "mysql:unix_socket=$unixSocket;dbname=$dbname" : "mysql:host=$host;dbname=$dbname";
        return PDOFactory::newConnectionDsn($dsn, $username, $password);
    }
}
