<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Infrastructure\Domain\Model\Obstacle;

use App\MarsRoverMission\Domain\Model\Obstacle\ObstacleRepository;
use App\MarsRoverMission\Domain\Model\Obstacle\Obstacle;
use App\MarsRoverMission\Domain\Model\Obstacle\ObstacleId;
use App\MarsRoverMission\Domain\Model\Obstacle\ObstaclePosition;

class ObstacleRepositoryPDO implements ObstacleRepository
{
    private \PDO $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function save(Obstacle $obstacle): void
    {
        $this->pdo->prepare('
            INSERT INTO obstacle (id, position) 
            VALUES (:id, :position)'
        )
            ->execute([
                'id' => $obstacle->obstacleId()->id(),
                'position' => $obstacle->position()->toJson()
            ]);
    }

    public function loadAll(): array
    {
        $q = $this->pdo->prepare(
            'SELECT * from obstacle'
        );
        $q->execute();
        $rows = $q->fetchAll(\PDO::FETCH_ASSOC);
        return $this->buildArrayOfObstacles($rows);
    }

    public function loadById(ObstacleId $obstacleId): Obstacle
    {
        $q = $this->pdo->prepare(
            'SELECT * from obstacle
            WHERE id = :id'
        );
        $q->execute(['id' => $obstacleId->id()]);
        $result = $q->fetch(\PDO::FETCH_ASSOC);
        return $this->buildObstacleEntity($result);
    }

    private function buildArrayOfObstacles(array $rows): array
    {
        $array = [];
        foreach ($rows as $obstacle)
        {
            $position = json_decode($obstacle['position']);
            $array[] = Obstacle::create(
                ObstacleId::fromId($obstacle['id']),
                ObstaclePosition::fromXY(
                    $position->x,
                    $position->y
                )
            );
        }
        return $array;
    }

    private function buildObstacleEntity(array $array): Obstacle
    {
        $position = json_decode($array['position']);
        return $rover = Obstacle::create(
            ObstacleId::fromId($array['id']),
            ObstaclePosition::fromXY(
                $position->x,
                $position->y
            )
        );
    }

    public function truncate()
    {
        $this->pdo->prepare('TRUNCATE TABLE obstacle')->execute();
    }
}
