<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Infrastructure\Domain\Model\Rover;

use App\MarsRoverMission\Domain\Model\Rover\Rover;
use App\MarsRoverMission\Domain\Model\Rover\RoverDirection;
use App\MarsRoverMission\Domain\Model\Rover\RoverId;
use App\MarsRoverMission\Domain\Model\Rover\RoverPosition;
use App\MarsRoverMission\Domain\Model\Rover\RoverRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RoverRepositoryPDO implements RoverRepository
{
    private \PDO $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function save(Rover $rover): void
    {
        $this->pdo->prepare('
            INSERT INTO rover (id, direction, position) 
            VALUES (:id, :direction, :position)'
        )
            ->execute([
                'id' => $rover->roverId()->id(),
                'direction' => $rover->direction()->value(),
                'position' => $rover->position()->toJson()
            ]);
    }

    public function loadAll(): array
    {
        $q = $this->pdo->prepare(
            'SELECT * from rover'
        );
        $q->execute();
        $rows = $q->fetchAll(\PDO::FETCH_ASSOC);
        return $this->buildArrayOfRovers($rows);
    }


    public function loadById(RoverId $roverId): Rover
    {
        $q = $this->pdo->prepare(
            'SELECT * from rover
            WHERE id = :id'
        );
        $q->execute(['id' => $roverId->id()]);
        $result = $q->fetch(\PDO::FETCH_ASSOC);
        if (!$result) {
            throw new NotFoundHttpException('Rover with ID ' . $roverId->id() . ' not found.');
        }
        return $this->buildRoverEntity($result);
    }

    private function buildArrayOfRovers(array $rows): array
    {
        $array = [];
        foreach ($rows as $rover)
        {
            $array[] = $this->buildRoverEntity($rover);
        }
        return $array;
    }

    private function buildRoverEntity(array $array): Rover
    {
        $position = json_decode($array['position']);
         return $rover = Rover::create(
                RoverId::fromId($array['id']),
                RoverDirection::fromDirection($array['direction']),
                RoverPosition::fromXY(
                    $position->x,
                    $position->y
                )
            );
    }

    public function update(Rover $rover): void
    {
        $this->pdo->prepare('
            UPDATE rover
            SET direction = :direction,
            position = :position
            WHERE id = :id'
        )
            ->execute([
                'id' => $rover->roverId()->id(),
                'direction' => $rover->direction()->value(),
                'position' => $rover->position()->toJson()
            ]);
    }

    public function truncate()
    {
        $this->pdo->prepare('TRUNCATE TABLE rover')->execute();
    }
}
