<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Domain\Model\Rover;

class RoverPosition
{
    private int $x;
    private int $y;

    private function __construct(int $x, int $y)
    {
        $this->x = $x;
        $this->y = $y;
        $this->guard();
    }

    public static function fromXY(int $x, int $y): RoverPosition
    {
        return new self ($x, $y);
    }

    public function xPosition(): int
    {
        return $this->x;
    }
    public function yPosition(): int
    {
        return $this->y;
    }

    private function guard(): void
    {
        if ($this->x > 100) {
            throw PlanetLimitsException::fromPosition($this);
        }
        if ($this->x < -100) {
            throw PlanetLimitsException::fromPosition($this);
        }
        if ($this->y > 100) {
            throw PlanetLimitsException::fromPosition($this);
        }
        if ($this->y < -100) {
            throw PlanetLimitsException::fromPosition($this);
        }
    }

    public function toJson()
    {
        return json_encode(['x' => $this->xPosition(), 'y' => $this->yPosition()]);
    }

    public function xIncrease(): void
    {
        $this->x = $this->x + 1;
    }

    public function xDecrease(): void
    {
        $this->x = $this->x - 1;
    }

    public function yIncrease(): void
    {
        $this->y = $this->y + 1;
    }

    public function yDecrease(): void
    {
        $this->y = $this->y - 1;
    }

    public function fromJson(string $lastPosition): void
    {
        $this->x = json_decode($lastPosition)->x;
        $this->y = json_decode($lastPosition)->y;
    }
}
