<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Domain\Model\Rover;

interface RoverRepository
{
    public function save(Rover $rover): void;

    /**
     * @return Rover[]
     */
    public function loadAll(): array;

    public function loadById(RoverId $roverId): Rover;

    public function update(Rover $rover): void;
}
