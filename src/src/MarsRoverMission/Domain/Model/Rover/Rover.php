<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Domain\Model\Rover;

class Rover
{
    private RoverId $roverId;
    private RoverDirection $direction;
    private RoverPosition $position;

    private function __construct(RoverId $userId, RoverDirection $direction, RoverPosition $position)
    {
        $this->roverId = $userId;
        $this->direction = $direction;
        $this->position = $position;
    }

    public static function create(RoverId $userId, RoverDirection $direction, RoverPosition $position) : Rover
    {
        return new self($userId, $direction, $position);
    }

    public function roverId(): RoverId
    {
        return $this->roverId;
    }

    public function direction(): RoverDirection
    {
        return $this->direction;
    }

    public function position(): RoverPosition
    {
        return $this->position;
    }

    public function move(string $move): Rover
    {
        if ($move == 'F')
        {
            $this->moveForward();
        }

        if ($move == 'L')
        {
            $this->moveLeft();
        }

        if ($move == 'R')
        {
            $this->moveRight();
        }

        return new self($this->roverId, $this->direction, $this->position);

    }

    private function moveForward(): void
    {
        switch ($this->direction->value())
        {
            case 'N':
                $this->position->yIncrease();
                break;
            case 'S':
                $this->position->yDecrease();
                break;
            case 'E':
                $this->position->xIncrease();
                break;
            case 'W':
                $this->position->xDecrease();
                break;
        }
    }

    private function moveLeft(): void
    {
        switch ($this->direction->value())
        {
            case 'N':
                $this->position->xDecrease();
                $this->direction->west();
                break;
            case 'S':
                $this->position->xIncrease();
                $this->direction->east();
                break;
            case 'E':
                $this->position->yIncrease();
                $this->direction->north();
                break;
            case 'W':
                $this->position->yDecrease();
                $this->direction->south();
                break;
        }
    }

    private function moveRight()
    {
        switch ($this->direction->value())
        {
            case 'N':
                $this->position->xIncrease();
                $this->direction->east();
                break;
            case 'S':
                $this->position->xDecrease();
                $this->direction->west();
                break;
            case 'W':
                $this->position->yIncrease();
                $this->direction->north();
                break;
            case 'E':
                $this->position->yDecrease();
                $this->direction->south();
                break;
        }
    }
}
