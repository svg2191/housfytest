<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Domain\Model\Rover;

class RoverDirection
{
    private string $direction;

    private function __construct(string $direction)
    {
        $this->direction = $direction;
        $this->guard();
    }

    public static function fromDirection($direction): RoverDirection
    {
        return new self ($direction);
    }

    public function value(): string
    {
        return $this->direction;
    }

    private function guard(): void
    {
        if (!in_array($this->direction, ['N', 'S', 'E', 'W']))
        {
            throw UnknownDirectionException::fromDirection($this);
        }
    }

    public function west(): void
    {
        $this->direction = 'W';
    }

    public function east(): void
    {
        $this->direction = 'E';
    }

    public function north(): void
    {
        $this->direction = 'N';
    }

    public function south(): void
    {
        $this->direction = 'S';
    }
}
