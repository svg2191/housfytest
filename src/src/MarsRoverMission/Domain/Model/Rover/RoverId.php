<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Domain\Model\Rover;

class RoverId
{
    private string $id;

    private function __construct(string $id)
    {
        $this->id = $id;
    }

    public static function fromId(string $id)
    {
        return new self($id);
    }

    public function id(): string
    {
        return $this->id;
    }
}
