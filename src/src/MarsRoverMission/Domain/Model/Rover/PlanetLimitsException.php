<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Domain\Model\Rover;

use App\MarsRoverMission\Domain\Model\Obstacle\ObstaclePosition;

class PlanetLimitsException extends \Exception
{

    public static function fromPosition(RoverPosition $position): self
    {
        return new self("Your Rover exceeds the limits of the planet (x and y should be between -100 to 100). You are at [" . $position->xPosition() . "," . $position->yPosition() . "].");
    }

    public static function fromObstaclePosition(ObstaclePosition $position): self
    {
        return new self("Your Obstacle exceeds the limits of the planet (x and y should be between -100 to 100). You are at [" . $position->xPosition() . "," . $position->yPosition() . "].");
    }
}
