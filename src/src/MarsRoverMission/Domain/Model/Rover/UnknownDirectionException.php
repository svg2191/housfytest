<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Domain\Model\Rover;


class UnknownDirectionException extends \Exception
{
    public static function fromDirection(RoverDirection $direction): self
    {
        return new self("Unknown Direction " . $direction->value() . ".Only Allowed ''N,S,E,W'.");
    }
}
