<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Domain\Model\Obstacle;

use App\MarsRoverMission\Domain\Model\Rover\PlanetLimitsException;

class ObstaclePosition
{
    private int $x;
    private int $y;

    private function __construct(int $x, int $y)
    {
        $this->x = $x;
        $this->y = $y;
        $this->guard();
    }

    public static function fromXY(int $x, int $y): ObstaclePosition
    {
        return new self ($x, $y);
    }

    public static function fromJson(string $json): ObstaclePosition
    {
        $object = json_decode($json);
        return new self ($object->x, $object->y);
    }

    public function xPosition(): int
    {
        return $this->x;
    }
    public function yPosition(): int
    {
        return $this->y;
    }

    private function guard(): void
    {
        if ($this->x > 100) {
            throw PlanetLimitsException::fromObstaclePosition($this);
        }
        if ($this->x < -100) {
            throw PlanetLimitsException::fromObstaclePosition($this);
        }
        if ($this->y > 100) {
            throw PlanetLimitsException::fromObstaclePosition($this);
        }
        if ($this->y < -100) {
            throw PlanetLimitsException::fromObstaclePosition($this);
        }
    }

    public function toJson()
    {
        return json_encode(['x' => $this->xPosition(), 'y' => $this->yPosition()]);
    }
}
