<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Domain\Model\Obstacle;

class ObstacleId
{
    private string $id;

    private function __construct(string $id)
    {
        $this->id = $id;
    }

    public static function fromId(string $id): ObstacleId
    {
        return new self($id);
    }

    public function id(): string
    {
        return $this->id;
    }
}
