<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Domain\Model\Obstacle;

class Obstacle
{
    private ObstacleId $obstacleId;
    private ObstaclePosition $position;

    private function __construct(ObstacleId $userId, ObstaclePosition $position)
    {
        $this->obstacleId = $userId;
        $this->position = $position;
    }

    public static function create(ObstacleId $userId, ObstaclePosition $position): Obstacle
    {
        return new self($userId, $position);
    }

    public function obstacleId(): ObstacleId
    {
        return $this->obstacleId;
    }


    public function position(): ObstaclePosition
    {
        return $this->position;
    }
}
