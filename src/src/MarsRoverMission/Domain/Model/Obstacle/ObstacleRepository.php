<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Domain\Model\Obstacle;

use App\MarsRoverMission\Domain\Model\Obstacle\Obstacle;

interface ObstacleRepository
{
    public function save(Obstacle $rover): void;

    /**
     * @return Obstacle[]
     */
    public function loadAll(): array;

    public function loadById(ObstacleId $obstacleId): Obstacle;
}
