<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Application\Service\Obstacle;

use App\MarsRoverMission\Domain\Model\Obstacle\Obstacle;

class ListObstacleResponse
{
    private array $list;

    public function __construct(Obstacle ...$list)
    {
        $this->list = $list;
    }

    public function buildResponseToArray(): array
    {
        $responseArray = [];
        foreach ($this->list as $key => $obstacle)
        {
            $responseArray[$key]['id'] = $obstacle->obstacleId()->id();
            $responseArray[$key]['position']['x'] = $obstacle->position()->xPosition();
            $responseArray[$key]['position']['y'] = $obstacle->position()->xPosition();
        }
        return $responseArray;
    }
}
