<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Application\Service\Obstacle;

use App\MarsRoverMission\Domain\Model\Obstacle\ObstacleId;

class CreateObstacleResponse
{
    private ObstacleId $obstacleId;

    public function __construct(ObstacleId $obstacleId)
    {
        $this->obstacleId = $obstacleId;
    }

    public function toString(): string
    {
        return 'Created Obstacle with ID: ' . $this->obstacleId->id();
    }
}
