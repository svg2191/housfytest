<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Application\Service\Obstacle;

use App\MarsRoverMission\Domain\Model\Obstacle\ObstacleRepository;

class ListObstacleUseCase
{
    private ObstacleRepository $repository;

    public function __construct(ObstacleRepository $repository)
    {
        $this->repository = $repository;
    }

    public function execute(ListObstacleCommand $command): ListObstacleResponse
    {
        $list = $this->repository->loadAll();
        return new ListObstacleResponse(...$list);
    }
}
