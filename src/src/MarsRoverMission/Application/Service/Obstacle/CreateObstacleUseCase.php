<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Application\Service\Obstacle;



use App\MarsRoverMission\Domain\Model\Obstacle\Obstacle;
use App\MarsRoverMission\Domain\Model\Obstacle\ObstacleId;
use App\MarsRoverMission\Domain\Model\Obstacle\ObstaclePosition;
use App\MarsRoverMission\Domain\Model\Obstacle\ObstacleRepository;

class CreateObstacleUseCase
{
    private ObstacleRepository $obstacleRepository;

    public function __construct(ObstacleRepository $obstacleRepository)
    {
        $this->obstacleRepository = $obstacleRepository;
    }

    public function execute(CreateObstacleCommand $command): CreateObstacleResponse
    {
        $rover = Obstacle::create(
            ObstacleId::fromId($command->obstacleId()),
            ObstaclePosition::fromJson($command->position()));
        $this->obstacleRepository->save($rover);
        return new CreateObstacleResponse($rover->obstacleId());
    }
}
