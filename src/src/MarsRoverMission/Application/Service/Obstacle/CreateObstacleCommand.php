<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Application\Service\Obstacle;

class CreateObstacleCommand
{
    private string $position;
    private string $obstacleId;

    public function __construct(string $position)
    {
        $this->obstacleId = uniqid();
        $this->position = $position;
    }

    public function position(): string
    {
        return $this->position;
    }

    /**
     * @return string
     */
    public function obstacleId(): string
    {
        return $this->obstacleId;
    }
}
