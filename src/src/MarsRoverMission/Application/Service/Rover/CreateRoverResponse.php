<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Application\Service\Rover;

use App\MarsRoverMission\Domain\Model\Rover\RoverId;

class CreateRoverResponse
{
    private RoverId $roverId;

    public function __construct(RoverId $roverId)
    {
        $this->roverId = $roverId;
    }

    public function toString(): string
    {
        return 'Created Rover with ID: ' . $this->roverId->id();
    }

}
