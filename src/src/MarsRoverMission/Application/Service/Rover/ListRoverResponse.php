<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Application\Service\Rover;

use App\MarsRoverMission\Domain\Model\Rover\Rover;

class ListRoverResponse
{
    private array $list;

    public function __construct(Rover ...$list)
    {
        $this->list = $list;
    }

    public function buildResponseToArray(): array
    {
        $responseArray = [];
        foreach ($this->list as $key => $rover)
        {
            $responseArray[$key]['id'] = $rover->roverId()->id();
            $responseArray[$key]['direction'] = $rover->direction()->value();
            $responseArray[$key]['position']['x'] = $rover->position()->xPosition();
            $responseArray[$key]['position']['y'] = $rover->position()->xPosition();
        }
        return $responseArray;
    }
}
