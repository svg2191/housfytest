<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Application\Service\Rover;

use App\MarsRoverMission\Domain\Model\Rover\RoverRepository;

class ListRoverUseCase
{
    private RoverRepository $repository;

    public function __construct(RoverRepository $repository)
    {
        $this->repository = $repository;
    }

    public function execute(ListRoverCommand $command): ListRoverResponse
    {
        $list = $this->repository->loadAll();
        return new ListRoverResponse(...$list);
    }
}
