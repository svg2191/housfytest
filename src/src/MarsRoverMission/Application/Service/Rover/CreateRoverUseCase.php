<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Application\Service\Rover;

use App\MarsRoverMission\Domain\Model\Rover\Rover;
use App\MarsRoverMission\Domain\Model\Rover\RoverDirection;
use App\MarsRoverMission\Domain\Model\Rover\RoverId;
use App\MarsRoverMission\Domain\Model\Rover\RoverPosition;
use App\MarsRoverMission\Domain\Model\Rover\RoverRepository;

class CreateRoverUseCase
{
    const DEFAULT_DIRECTION = 'N';
    const DEFAULT_X_POSITION = 0;
    const DEFAULT_Y_POSITION = 0;

    private RoverRepository $roverRepository;

    public function __construct(RoverRepository $roverRepository)
    {
        $this->roverRepository = $roverRepository;
    }

    public function execute(CreateRoverCommand $command): CreateRoverResponse
    {
        $rover = Rover::create(
            RoverId::fromId($command->roverId()),
            RoverDirection::fromDirection(self::DEFAULT_DIRECTION),
            RoverPosition::fromXY(self::DEFAULT_X_POSITION, self::DEFAULT_Y_POSITION));
        $this->roverRepository->save($rover);
        return new CreateRoverResponse($rover->roverId());
    }
}
