<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Application\Service\Rover;

use App\Common\Application\Bus\Command\Command;

class MoveRoverCommand implements Command
{
    private string $roverId;
    private array $moves;

    public function __construct(string $roverId, array $moves)
    {
        $this->roverId = $roverId;
        $this->moves = $moves;
    }

    public function roverId(): string
    {
        return $this->roverId;
    }

    public function moves(): array
    {
        return $this->moves;
    }
}
