<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Application\Service\Rover;

use App\MarsRoverMission\Domain\Model\Rover\RoverId;
use App\MarsRoverMission\Domain\Model\Rover\RoverPosition;

class MoveRoverResponse
{
    private RoverPosition $position;
    private ?object $obstaclePosition;

    public function __construct(RoverPosition $position, ?string $obstaclePosition)
    {
        $this->obstaclePosition = $obstaclePosition ? json_decode($obstaclePosition) : null;
        $this->position = $position;
    }

    public function toString(): string
    {
        if (!is_null($this->obstaclePosition))
        {
            return 'Your Rover is at [' . $this->position->xPosition() . ',' . $this->position->yPosition() .']. Find an obstacle trying to move to [' . $this->obstaclePosition->x . ',' . $this->obstaclePosition->y . '].';
        }
        return 'Your Rover is at [' . $this->position->xPosition() . ',' . $this->position->yPosition() .']';
    }

}
