<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Application\Service\Rover;

use App\MarsRoverMission\Domain\Model\Obstacle\Obstacle;
use App\MarsRoverMission\Domain\Model\Obstacle\ObstacleRepository;
use App\MarsRoverMission\Domain\Model\Rover\Rover;
use App\MarsRoverMission\Domain\Model\Rover\RoverId;
use App\MarsRoverMission\Domain\Model\Rover\RoverRepository;

class MoveRoverUseCase
{
    private RoverRepository $roverRepository;
    private ObstacleRepository $obstacleRepository;

    public function __construct(RoverRepository $roverRepository, ObstacleRepository $obstacleRepository)
    {
        $this->roverRepository = $roverRepository;
        $this->obstacleRepository = $obstacleRepository;
    }

    public function execute(MoveRoverCommand $command): MoveRoverResponse
    {
        $rover = $this->roverRepository->loadById(RoverId::fromId($command->roverId()));
        $obstacles = $this->findObstaclesPosition(...$this->obstacleRepository->loadAll());
        $this->movingRover($rover, $command->moves(), $obstacles, $obstaclePosition);
        $this->roverRepository->update($rover);
        return new MoveRoverResponse($rover->position(), $obstaclePosition);
    }

    private function movingRover(Rover $rover, array $moves, array $obstacles, &$obstaclePosition): void
    {
        foreach ($moves as $move)
        {
            $lastPosition = $rover->position()->toJson();
            $rover->move($move);
            $newPosition = $rover->position()->toJson();
            if ($this->findObstacle($newPosition, $obstacles)) {
                $rover->position()->fromJson($lastPosition);
                $obstaclePosition = $newPosition;
                return;
            }
        }
    }

    private function findObstaclesPosition(Obstacle ...$obstacles): array
    {
        $positions = [];
        foreach ($obstacles as $obstacle){
            $positions[] = $obstacle->position()->toJson();
        }
        return $positions;
    }

    private function findObstacle(string $position, array $obstacles): bool
    {
        return in_array($position, $obstacles);
    }
}
