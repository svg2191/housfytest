<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Application\Service\Rover;

use App\Common\Application\Bus\Command\Command;

class CreateRoverCommand implements Command
{
    private string $roverId;

    public function __construct()
    {
        $this->roverId = uniqid();
    }

    public function roverId(): string
    {
        return $this->roverId;
    }
}
