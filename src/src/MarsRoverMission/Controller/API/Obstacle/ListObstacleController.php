<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Controller\API\Obstacle;

use App\MarsRoverMission\Application\Service\Obstacle\ListObstacleCommand;
use App\MarsRoverMission\Application\Service\Obstacle\ListObstacleUseCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ListObstacleController
{
    private ListObstacleUseCase $listObstacle;

    public function __construct(ListObstacleUseCase $listObstacle)
    {
        $this->listObstacle = $listObstacle;
    }

    public function __invoke(Request $request): JsonResponse
    {
        $response = $this->listObstacle->execute(new ListObstacleCommand());
        return JsonResponse::create($response->buildResponseToArray());
    }
}
