<?
declare(strict_types=1);

namespace App\MarsRoverMission\Controller\API\Obstacle;

use App\MarsRoverMission\Application\Service\Obstacle\CreateObstacleCommand;
use App\MarsRoverMission\Application\Service\Obstacle\CreateObstacleUseCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CreateObstacleController
{
    private CreateObstacleUseCase $createObstacle;

    public function __construct(CreateObstacleUseCase $createObstacle)
    {
        $this->createObstacle = $createObstacle;
    }

    public function __invoke(Request $request): Response
    {
        $this->guard($request);
        $response = $this->createObstacle->execute(new CreateObstacleCommand($request->request->get('position')));
        return Response::create($response->toString());
    }

    private function guard(Request $request)
    {
        if (!$request->request->has('position')) {
            throw new BadRequestHttpException('Missing required parameter position');
        }
    }
}
