<?
declare(strict_types=1);

namespace App\MarsRoverMission\Controller\API\Rover;

use App\MarsRoverMission\Application\Service\Rover\CreateRoverCommand;
use App\MarsRoverMission\Application\Service\Rover\CreateRoverUseCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CreateRoverController
{
    private CreateRoverUseCase $createRover;

    public function __construct(CreateRoverUseCase $createRover)
    {
        $this->createRover = $createRover;
    }

    public function __invoke(Request $request): Response
    {
        $response = $this->createRover->execute(new CreateRoverCommand());
        return Response::create($response->toString());
    }

}
