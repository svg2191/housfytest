<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Controller\API\Rover;

use App\MarsRoverMission\Application\Service\Rover\MoveRoverCommand;
use App\MarsRoverMission\Application\Service\Rover\MoveRoverUseCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class MoveRoverController
{
    private MoveRoverUseCase $moveRover;

    public function __construct(MoveRoverUseCase $moveRover)
    {
        $this->moveRover = $moveRover;
    }

    public function __invoke(Request $request): Response
    {
        $this->guard($request);
        $response = $this->moveRover->execute(
            new MoveRoverCommand(
                $request->attributes->get('roverId'),
                str_split($request->request->get('moves'))
            )
        );
        return Response::create($response->toString());
    }

    private function guard(Request $request)
    {
        if (!$request->request->has('moves')) {
            throw new BadRequestHttpException('Missing required parameter moves');
        }
        $moves = str_split($request->request->get('moves'));
        foreach ($moves as $move){
            if (!in_array($move, ['F','R','L'])) {
                throw new BadRequestHttpException('Move ' . $move . 'not allowed');
            }
        }

    }
}
