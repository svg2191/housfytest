<?php
declare(strict_types=1);

namespace App\MarsRoverMission\Controller\API\Rover;

use App\MarsRoverMission\Application\Service\Rover\ListRoverCommand;
use App\MarsRoverMission\Application\Service\Rover\ListRoverUseCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ListRoverController
{
    private ListRoverUseCase $listRover;

    public function __construct(ListRoverUseCase $listRover)
    {
        $this->listRover = $listRover;
    }

    public function __invoke(Request $request): JsonResponse
    {
        $response = $this->listRover->execute(new ListRoverCommand());
        return JsonResponse::create($response->buildResponseToArray());
    }
}
