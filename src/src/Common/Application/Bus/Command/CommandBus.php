<?php
declare(strict_types=1);

namespace App\Common\Application\Bus\Command;

use App\Common\Application\Bus\Query\Response;

interface CommandBus
{
    public function dispatch(Command $command): ?Response;
}
