<?php
declare(strict_types=1);

namespace App\Common\Application\Bus\Query;

interface Response
{
    public function toArray(): array;
}
