<?php
declare(strict_types=1);

namespace App\Common\Infrastructure\Bus\Query;

use App\Common\Application\Bus\Query\Query;
use League\Tactician\CommandBus as TacticianBus;
use App\Common\Application\Bus\Query\QueryBus;
use App\Common\Application\Bus\Query\Response;

class TacticianQueryBus implements QueryBus
{
    private TacticianBus $bus;

    public function __construct(TacticianBus $bus)
    {
        $this->bus = $bus;
    }

    public function ask(Query $query): ?Response
    {
        return $this->bus->handle($query);
    }
}
