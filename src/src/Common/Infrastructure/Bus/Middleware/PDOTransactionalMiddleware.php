<?php
declare(strict_types=1);

namespace App\Common\Infrastructure\Bus\Middleware;

class PDOTransactionalMiddleware
{
    private $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param object $command
     * @param callable $next
     *
     * @return mixed
     */
    public function execute($command, callable $next)
    {
        $this->pdo->beginTransaction();

        try {
            $returnValue = $next($command);
            $this->pdo->commit();
            return $returnValue;
        }
        catch (\Throwable $e) {
            $this->pdo->rollBack();
            throw $e;
        }
    }
}
