<?php
declare(strict_types=1);

namespace App\Common\Infrastructure\Bus;

use Psr\Container\ContainerInterface;

class ContainerOpinionatedLocator
{
    protected ContainerInterface $container;
    private string $commandSuffix;
    private string $commandHandlerSuffix;

    public function __construct(
        ContainerInterface $container,
        string $commandSuffix,
        string $commandHandlerSuffix
    ) {
        $this->container = $container;
        $this->commandSuffix = $commandSuffix;
        $this->commandHandlerSuffix = $commandHandlerSuffix;
    }

    public function getHandlerForCommand($commandName)
    {
        $serviceId = preg_replace('/' . $this->commandSuffix . '$/', $this->commandHandlerSuffix, $commandName);

        return $this->container->get($serviceId);
    }
}
