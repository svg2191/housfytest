<?php
declare(strict_types=1);

namespace App\Common\Infrastructure\Bus\Command;

use League\Tactician\CommandBus as TacticianBus;
use App\Common\Application\Bus\Command\Command;
use App\Common\Application\Bus\Command\CommandBus;
use App\Common\Application\Bus\Query\Response;

class TacticianCommandBus implements CommandBus
{
    private TacticianBus $bus;

    public function __construct(TacticianBus $bus)
    {
        $this->bus = $bus;
    }

    public function dispatch(Command $command): ?Response
    {
        return $this->bus->handle($command);
    }
}
