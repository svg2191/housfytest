# Symfony 5 docker containers

Prueba de nivel para Housfy. 
Swagger de la API en /src/doc/mars-rover-mission-swagger.yml

```
git clone git@gitlab.com:svg2191/housfytest.git

cd housfytest

cd docker

docker-compose up

docker-compose run php-fpm bin/console doctrine:migrations:migrate
```

## Compose

### Database (MariaDB)

...

### PHP (PHP-FPM)

Composer is included

```
docker-compose run php-fpm composer 
```

To run fixtures

```
docker-compose run php-fpm bin/console doctrine:fixtures:load
```

### Webserver (Nginx)

...
